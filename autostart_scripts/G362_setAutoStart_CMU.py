## Code provided by EPSON support engineer Raymond <rchow@eea.epson.com> ##
## Read - README for instructions ##
## Code written for Python2x not Python3x ##
## Dependency - 3rd party module download at https://pypi.python.org/pypi/pyserial ##

#!/usr/bin/env python

import serial # 3rd party module download at https://pypi.python.org/pypi/pyserial
import struct
import string
import time
import argparse
import os
import imu_g362_drv as imu
#from imu_def import *

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--serial_port", help="specifies COMxx or /dev/ttyUSBx to use", type=str, default="")
parser.add_argument("-c", "--current_baud_rate", help="specifies current baudrate to use", type=int, choices=[460800, 230400, 115200],default=460800)
parser.add_argument("-n", "--new_baud_rate", help="specifies new baudrate to program to", type=int, choices=[460800, 230400, 115200],default=460800)
parser.add_argument("-d", "--drate", help="specifies output data rate in sps, otherwise \
                    use default of 1000sps", type=float, choices=[2000, 1000, 500, 250, 125, 62.5, 31.25], default=250)
parser.add_argument("-a", "--autostart", help="specifies enable autostart mode", action="store_true")
parser.add_argument("-l", "--listserial", help="specifies to list of available COM ports", action="store_true")
args = parser.parse_args()

def set_mv_avg_filter(datarate):
    if datarate == 2000:
        mv_avg_filter = "MV_AVG2"
    elif datarate == 1000:
        mv_avg_filter = "MV_AVG4"
    elif datarate == 500:
        mv_avg_filter = "MV_AVG8"
    elif datarate == 250:
        mv_avg_filter = "MV_AVG16"
    elif datarate == 125:
        mv_avg_filter = "MV_AVG32"
    elif datarate == 62.5:
        mv_avg_filter = "MV_AVG64"
    else:
        mv_avg_filter = "MV_AVG128"
    return mv_avg_filter
        
def main():
    if (args.listserial or args.serial_port == ""):
        print ("Available COM ports:", list(imu.serial_ports()))
    else:
        try:
            imu.init_serial(args.serial_port,args.current_baud_rate) # use /dev/ttyUSBx for Linux
            imu.gotoMode('Config')
            time.sleep(1)
            imu.initcheck()
            imu.selftest()
            imu.flashtest()
            imu.setBaudRate(args.new_baud_rate)
            imu.close_serial()
            imu.init_serial(args.serial_port,args.new_baud_rate) # use /dev/ttyUSBx for Linux
            imu.setOutputRate(args.drate)
            mv_avg_filter = "MV_AVG32"
            imu.setFilter(mv_avg_filter)
            if (args.autostart):
                imu.setUartMode(3)
            else:
                imu.setUartMode(1)
            imu.setFlagOut(1)
            imu.setTempOut(1)
            imu.setGyroOut(1)
            imu.setAcclOut(1)
            imu.setGpioOut(1)
            imu.setCountOut(1)
            imu.setChksmOut(1)
            imu.setTemp32Out(0)
            imu.setGyro32Out(0)
            imu.setAccl32Out(0)
            imu.setExtSel('Counter')
            imu.setExtPolarity(0)
            imu.flashbackup() # No need to wearout NVRAM write cycles
            imu.close_serial()
        except serial.SerialException:
            print ("Serial COM Error:", list(imu.serial_ports()))
 
if __name__ == "__main__": 
    main()