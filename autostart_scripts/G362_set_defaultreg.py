#!/usr/bin/env python

import serial # 3rd party module download at https://pypi.python.org/pypi/pyserial
import struct
import string
import time
import argparse
import os
import imu_g362_drv as imu
#from imu_def import *

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--serial_port", help="specifies COMxx or /dev/ttyUSBx to use", type=str, default="")
parser.add_argument("-b", "--baud_rate", help="specifies baudrate to use", type=int, choices=[460800, 230400, 115200], default=460800)
parser.add_argument("-t", "--sec_duration", help="specifies time duration of execution in seconds",type=float, default=10)
parser.add_argument("-o", "--ofilename", help="specifies full filename with extension to override internally generated \
                    filename for CSV log", type=str,default="")
parser.add_argument("-d", "--drate", help="specifies output data rate in sps, otherwise \
                    use default of 1000sps", type=float, choices=[2000, 1000, 500, 250, 125, 62.5, 31.25], default=1000)
parser.add_argument("-l", "--listserial", help="specifies to list of available COM ports", action="store_true")
args = parser.parse_args()

if args.drate:
    drate = args.drate
    if args.drate == 2000:
        mv_avg_filter = "MV_AVG2"
    elif args.drate == 1000:
        mv_avg_filter = "MV_AVG4"
    elif args.drate == 500:
        mv_avg_filter = "MV_AVG8"
    elif args.drate == 250:
        mv_avg_filter = "MV_AVG16"
    elif args.drate == 125:
        mv_avg_filter = "MV_AVG32"
    elif args.drate == 62.5:
        mv_avg_filter = "MV_AVG64"
    else:
        mv_avg_filter = "MV_AVG128"

if (args.listserial or args.serial_port == ""):
    print ("Available COM ports:", list(imu.serial_ports()))
else:
    imu.init_serial(args.serial_port, args.baud_rate) # use /dev/ttyUSBx for Linux
    imu.initcheck()
    imu.selftest()
    imu.softreset()
    imu.regdump()
    imu.setExtSel('GPIO')
    imu.setOutputRate(1000)
    imu.setFilter('MV_AVG4')
    imu.setBaudRate(0)
    imu.setUartMode(0)
    imu.setFlagOut(1)
    imu.setTempOut(1)
    imu.setGyroOut(1)
    imu.setAcclOut(1)
    imu.setGpioOut(1)
    imu.setCountOut(1)
    imu.setChksmOut(0)
    imu.setTemp32Out(1)
    imu.setGyro32Out(1)
    imu.setAccl32Out(1)
    imu.setExtPolarity(0)
    imu.regdump()
    imu.flashbackup() # No need to wearout NVRAM write cycles
    imu.close_serial()
