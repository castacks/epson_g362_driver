#ifndef __EPSON_G362_H__
#define __EPSON_G362_H__

#include <unistd.h>                 // UNIX standard function definitions
#include <fcntl.h>                  // File control definitions
#include <termios.h>                // POSIX terminal control definitionss
#include <iostream>                 // For cout
#include <stdio.h>                  // For printf


#include <ros/ros.h>
// constants
#define CARRIAGE_RETURN 0x0D
#define MAX_COUNT 1397861550
#define ALMOST_ROLLOVER 1300000000
#define ONE_SEC_NSEC 1000000000
#define HALF_SEC_NSEC 500000000
#define PER_COUNT_NSEC 21330
#define GYRO_SCALING_FACTOR 0.005*0.01745
#define ACC_SCALING_FACTOR 0.000125*9.80665
#define TEMP_SCALE_FACTOR 0.0042725
#define TEMP_CONST1 15214
#define TEMP_CONST2 25
// gravity factor *9.80665
// degree to radian conversion *0.01745
namespace ca
{
class Epson_G362{
 public:

  Epson_G362():
  fd_(-1),
  chksum_read_(1){}

  ~Epson_G362() {close(fd_);}// Destructor


  bool Initialize(std::string portname);
  bool ReadFrame(uint8_t buffer[]);
  int OpenPort(std::string portname);
  bool ConfigurePort(int timeout);
  bool VerifyChecksum(uint8_t buffer[]);

 private:
  int fd_; // file descriptor
//  unsigned char buffer[22];
  unsigned char rbuffer_[1];
  uint16_t chksum_read_;
//  uint16_t chksum_cal_;
};

bool Epson_G362::Initialize(std::string portname){
  fd_ = OpenPort(portname);
  bool status = ConfigurePort(5); // configure serial port with timeout of 500ms (5x100ms)
  if(!status){
    ROS_ERROR_STREAM("Unable to open specified serial port");
    return false; // Exit if unable to open serial port
  }
  else{
    ROS_INFO_STREAM("Serial Port: OPEN\nParameters rejected, IMU in Autostart mode\n");
    tcflush(fd_, TCIOFLUSH); // Flush old data
    return true; // No error
  }
}

int Epson_G362::OpenPort(std::string portname){
  const char * port_name = portname.c_str();  // convert std::string to const char* for passing it to opening port
  fd_ = open(port_name, O_RDWR | O_NOCTTY | O_NDELAY);
  if(fd_ == -1) // if open is unsucessful
  {
    ROS_INFO_STREAM("Error in opening port ");
  }
  else
  {
    fcntl(fd_, F_SETFL, 0); // Clear all flags of fild descriptor (fd_)
  }
  return fd_;
} // Open_port

bool Epson_G362::ConfigurePort(int timeout){
  bool status=0;
  if(fd_<0){	// check if port is open
    return false;	// Port is not opened successfully
  }
  else        // Execute configure only if the port is open
  {
    struct termios port_settings;      		// structure to store the port settings in
    fcntl(fd_, F_SETFL);            			// Configure port reading
    tcgetattr(fd_, &port_settings);
    cfsetispeed(&port_settings, B460800);   // set input baud rate
    cfsetospeed(&port_settings, B460800);  	// set output baud rate
    // SET RAW MODE
    port_settings.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    port_settings.c_oflag &= ~OPOST;
    port_settings.c_lflag &= ~( ECHONL | ICANON | ISIG | IEXTEN);
    port_settings.c_cflag &= ~(CSIZE | PARENB);
    port_settings.c_cflag |= CS8;
    // End of RAW mode setting
    // SET READ TIMEOUT
    port_settings.c_cc[VTIME] = timeout;  			// Timeout - 500ms (5* 100ms) if VMIN = 0
    port_settings.c_cc[VMIN] = 0;   			// Minimum number of bytes to read
    port_settings.c_cflag |= (CLOCAL | CREAD);	// Enable receiver
    tcsetattr(fd_, TCSANOW, &port_settings);  	// apply the settings to the port
    if(fd_>0){// check serial port configuration status
      return true;// return no error
    }
    else{
      return false; // return error	
      ROS_ERROR_STREAM("Unable to Configure specified serial port");
    }
  }
}

bool Epson_G362::ReadFrame(uint8_t buffer[]){
  for(int k = 0; k<5; k++){
    for (int i=0; i<24; i++){ // read 24 bytes dataframe
      if(read(fd_,rbuffer_,1)){
        buffer[i] = rbuffer_[0];
      }
      else{
        ROS_ERROR_STREAM("Serial Port: Read Timeout!");
        return 0;
      }
    }

    if(VerifyChecksum(buffer)) // verify checksum
      return true;
    else{ // if checksum check failed, fix the frame
      for (int i=0; i<50; i++){  
      // look for frame ending i.e. 0x0D / carriage return
        if(read(fd_,rbuffer_,1))
          if(rbuffer_[0] == CARRIAGE_RETURN)   
          // found carriage return (0x0D), next 22 bytes is a frame
            break;
      }
    }
  }
  return false;
}

bool Epson_G362::VerifyChecksum(uint8_t buffer[]){
  uint16_t chksum_cal = 0;
  for(int i = 1;i<21;)        // Calculate Checksum
  {
    chksum_cal += ((buffer[i]<<8)+buffer[i+1]);
    i = i+2;
  }
  chksum_read_ = ((buffer[21]<<8)+buffer[22]);  // Read Checksum
  if(chksum_cal == chksum_read_)
    return true;
  else
    return false;
}


}
#endif
