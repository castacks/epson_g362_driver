# README #

### What is this repository for? ###

* Quick summary
This code provides interface between Epson IMU G362 and ROS.

* Version 2.0 (IMU Autostart)

### How do I get set up? ###

* Summary of set up
To get the IMU to work, first IMU needs to be configured in Autostart mode with appropriate sampling rate using a python script.
Then edit the launch file to select the correct serial port to which IMU is connected, other settings in the current configuration doesn't work.
Run roslaunch epson_g362 epson_g362.launch

* Configuration
Running the Python Script  G362_setAutoStart_CMU.py ->
command : "python  G362_setAutoStart_CMU.py -s COM3 -d 250 -a" this command should be run from folder where python is installed (Python2x and serial library module required which can be downloaded at https://pypi.python.org/pypi/pyserial on a windows or other os - haven't tested).
Where 
COM3 - choose correct COM port to which IMU is connected
250 - replace with the correct samples per second value required
-a - required to set the IMU to Autostart Mode.

* Dependencies
IMU should be in Autostart mode - requires Python script to configure ( G362_setAutoStart_CMU )


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Sezal Jain
* Rajshekar Prabhakar (rprabhak@andrew.cmu.edu)
* Mascot team

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2015, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.