/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sezal@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <sensor_msgs/Imu.h>
#include <epson_g362_driver/epson_g362.h>

namespace ca
{
  class Epson_G362_Wrapper : public Epson_G362
  {
    /*Just publishing imu data as ros topic through this wrapper */
    public:
      sensor_msgs::Imu imu_data;
      ros::Publisher imu_data_pub;
      ros::NodeHandle imu_node_handle;
      
      void initialize_imu_publisher(){
        imu_data_pub = imu_node_handle.advertise<sensor_msgs::Imu>("/epson_g362/imu", 50);
      }
      virtual bool DecodeData(){
        uint8_t buf[24] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        uint16_t gpio = 0;
        int32_t count = 0;
        int32_t count_corrected = 0;
        int32_t count_old = 0;
        int32_t count_diff = 0;
        int32_t time_current = 0;
        int32_t time_old = 0;
        int32_t time_nsec_current = 0;
        int32_t count_corrected_old = 0;
        float Temp = 0;
        bool rollover = false;
        bool flag_imu_lead = false;
        // Read Data and convert into IMU
        if(ros::ok()){
          ROS_INFO_STREAM("IMU in sampling mode, data publishing...\n Press Ctrl+C to stop");
        }
        while(ros::ok()){
        bool read_success = ReadFrame(buf); // read the data frame from IMU
        if(read_success)
        {
          Temp =  (( (short)((buf[3]<<8)+buf[4]) ) + TEMP_CONST1)*TEMP_SCALE_FACTOR + TEMP_CONST2 ; // see datasheet p43
          // Temperature
          imu_data.angular_velocity.x = ((short)((buf[5]<<8)+buf[6])) * GYRO_SCALING_FACTOR; 
          // gyro X (degrees/sec)
          imu_data.angular_velocity.y = ((short)((buf[7]<<8)+buf[8])) * GYRO_SCALING_FACTOR;
          // gyro Y (degrees/sec)
          imu_data.angular_velocity.z = ((short)((buf[9]<<8)+buf[10])) * GYRO_SCALING_FACTOR;
          // gyro Z (degrees/sec)
          imu_data.linear_acceleration.x = ((short)((buf[11]<<8)+buf[12])) * ACC_SCALING_FACTOR;
          // accel X (G)
          imu_data.linear_acceleration.y = ((short)((buf[13]<<8)+buf[14])) * ACC_SCALING_FACTOR;
          // accel X (G)
          imu_data.linear_acceleration.z = ((short)((buf[15]<<8)+buf[16])) * ACC_SCALING_FACTOR;
          // accel X (G)
          gpio = ((buf[17]<<8)+buf[18]);
          count = (int32_t)((int)((buf[19]<<8)+buf[20])*PER_COUNT_NSEC); 

          //converting count into time 
          time_current = ros::Time::now().toSec();
          time_nsec_current = ros::Time::now().nsec;

          // Correct the count number for nano seconds incase the 
          // PPS didn't occur
          count_diff = count - count_old;
          if(count > ALMOST_ROLLOVER){ // if count is 1.3sec close to rollover, ALMOST ROLLOVER = 1,300,000,000 
            rollover = 1;	// Set flag - about to rollover
          }
          else if(count_diff<0){ 	// Just rolled over or PPS asserted
            if(rollover){ // If rollover add old numbers to current count 
                          // number and reset rollover flag
              count_diff = count + (MAX_COUNT - count_old); // count rolls over at MAX_COUNT = 1397861550 
            }
            else {  // No rollover, meaning IMU counter was successfully reset at 0th second 
              count_diff = count;
              count_corrected = 0;
            }
            rollover = 0;	// reset rollover flag
          }
           
          count_corrected = (count_corrected + count_diff) % ONE_SEC_NSEC;	
          // count between 0 and 1 sec (1B ns), for nano sec part
          // Correct the time in seconds //
          if(time_current != time_old && count_corrected > HALF_SEC_NSEC){ 	
          // Seconds event occured and nano sec clock is > 500ms 
            time_current = time_current - 1;  // seconds count should be set be same as before 
                                              // i.e. decrement the incremented value
          }
          else if((count_corrected-count_corrected_old) < 0 && time_nsec_current > HALF_SEC_NSEC){ 	
          // counter rollover / reset event, and system nsec > 500ms
            time_current = time_current + 1;  // increment current time in seconds by 1
            flag_imu_lead = 1;  // Set flag that IMU is leading system clock
          }
          else if(flag_imu_lead && time_nsec_current > HALF_SEC_NSEC){
            // IMU is leading and system nano sec clock is > 500ms
              time_current = time_current + 1;  // increment current time in seconds by 1
          }
          else{
            flag_imu_lead = 0;  // reset IMU leading flag as system nanosec 
                                // clock will be < 500ms and one of the events occurred
          }

          imu_data.header.stamp.nsec = count_corrected;
          imu_data.header.stamp.sec = time_current;
          imu_data.header.frame_id = "epson";
          time_old=time_current;
          count_old = count;
          count_corrected_old = count_corrected;
          //cout << time_current<< " " << count_corrected << "\t nsec =" << time_nsec_current << endl;
          imu_data_pub.publish(imu_data);
        } // If (read_success)
        else{
          ROS_INFO("timeout");
        }
      }// no system interrupt was received such as Ctrl+C   
    } // While
  }; // Class
} // ca 


int main(int argc, char **argv) {
  //Now start ROS
    ros::init(argc, argv, "epson_g362");
    ros::NodeHandle n;
    ros::NodeHandle np("~");

    // specifies COMxx or /dev/ttyUSBx to use", type=str, default=""
    std::string portname;

    if (!np.getParam("port",portname)) {  // rosparam set /epson_g362/port /dev/ttyUSB0
      ROS_ERROR_STREAM("Port not defined");
      return -1;
    }
    ROS_INFO_STREAM("Portname = \""<<portname<<"\"");

    //Parameters Loaded
    ca::Epson_G362_Wrapper epson;
    epson.initialize_imu_publisher();
    bool initialized = epson.Initialize(portname);
    if(initialized && ros::ok()){
      epson.DecodeData();
    }
  //Do we want to add artificial covariance for the imu data?
    return false;
}

